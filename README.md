# Neon X UI

<br>

# Clavier Virtuel en Vue.js

Ce projet est un clavier virtuel interactif construit avec Vue.js 3 et Vite. Il offre une interface utilisateur intuitive permettant aux utilisateurs de saisir du texte à l'aide d'un clavier virtuel affiché à l'écran.

## Caractéristiques

- Clavier virtuel interactif avec prise en charge des caractères alphanumériques et des symboles.
- Saisie de texte en temps réel avec rétroaction visuelle.
- Prise en charge des touches spéciales comme Backspace, Enter, Espace et Caps Lock.
- Design réactif adapté aux différents appareils (desktop, tablettes, smartphones).

## Technologies Utilisées

- [Vue.js 3](https://v3.vuejs.org/)
- [Vite](https://vitejs.dev/)
- HTML/CSS

## Installation et Exécution

Pour exécuter ce projet sur votre machine locale, suivez ces étapes :

1. Clonez le dépôt depuis GitLab :

   ```bash
   git clone https://gitlab.com/tgodfreyarul/neon-x-ui.git
   ```

2. Accédez au dossier du projet :

   ```bash
   cd vite-project
   ```

3. Installez les dépendances :

   ```bash
   npm install
   ```

4. Lancez le serveur de développement :

   ```bash
   npm run dev
   ```

5. Ouvrez votre navigateur et accédez à `http://localhost:3000`.

## Contribution

Ce projet est mis à disposition à des fins de visualisation, d'évaluation et de collaboration potentiel.

## Licence
None yet

---
